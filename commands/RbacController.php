<?php

namespace app\commands;

use Yii;
use yii\console\Controller;
/**
 * Инициализатор RBAC выполняется в консоли php yii rbac/init
 */
class RbacController extends Controller {

    public function actionInit() {
        $auth = Yii::$app->authManager;

        $auth->removeAll(); //На всякий случай удаляем старые данные из БД...

        // Создадим роли админа и редактора новостей
        $admin = $auth->createRole('Admins');
        $admin->description = 'Администраторы';
        $editor = $auth->createRole('Moderators');
        $editor->description = 'Редакторы';
        $user = $auth->createRole('Users');
        $user->description = 'Пользователи';

        // запишем их в БД
        $auth->add($admin);
        $auth->add($editor);
        $auth->add($user);



        // Теперь добавим наследования.
        $auth->addChild($editor, $user);
        $auth->addChild($admin, $editor);
        $auth->addChild($admin, $user);


        // Назначаем роль Admins пользователю с ID 1
        $auth->assign($admin, 1);

        // Назначаем роль Moderators пользователю с ID 2
        $auth->assign($editor, 2);

        // Назначаем роль Users пользователю с ID 3
        $auth->assign($user, 3);
    }
}