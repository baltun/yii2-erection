Yii2 Erection
========================

Goal and description
--------------------
This is an open source template, compilation, assembly of Yii2 application skeleton with already integrated needed for more projects functionality

Features
--------

this template is based on basic template of yii2

- yii2-user plugin for visual user management, registration, authorization with email confirmation and password restore
- yii2-admin visual RBAC management of roles, rules, assigments and so on
- rbac initialization of main users

плагины взяты отсюда: http://nex-otaku-ru.blogspot.com/2016/09/yii2-user-yii2-admin.html

ToDo
----
- database installer ??
- one migration for all used modules ??


Installation
---------

- клонировать репо: git clone https://agamaya@bitbucket.org/baltun/yii2-test.git
- выполнить установку модулей composer: composer update
- настроить конфиг БД на нужный адрес
- создать БД (или скопировать дамп существующей БД, если скопировать дамп, то нижеследующие действия выполнять не нужно)
    - выполнить действия по установке модуля yii2_admin: php yii migrate --migrationPath=@ yii/rbac/migrations 
      (здесь нашел, что можно делать так называемые Namespaced Migrations, которые позволяют выполнять сразу все миграции одной командой, но пока не заработало, поэтому делаем отдельными командами)
    - выполнить действия по установке модуля yii2_user: php yii migrate/up --migrationPath=@ vendor/dektrium/yii2-user/migrations
    - Выполнить миграции приложения: php yii migrate
    - зарегистрировать пользователя с именем admin (обязательно первым и с таким именем, он получит полные права)
    - зарегистрировать второго пользователя, который получит права модератора
    - зарегистрировать третьего пользователя, который получит права обычногоп пользователя
    - выполнить консольный скрипт инициализации прав пользователей: php yii rbac/init
    - наполнить БД тестовыми постами

Usage
-------------

- работа с постами
- работа с пользователями
- работа с ролями
- подписка/отписка на уведомления в профиле пользователя

права на действия выдаются в соответствии с ТЗ в зависимости от роли пользователя