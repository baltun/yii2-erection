<?php

use yii\db\Migration;

/**
 * Handles the creation of table `posts`.
 */
class m170206_125407_create_posts_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('posts', [
            'id' => $this->primaryKey(),
            'preview' => $this->string(),
            'text' => $this->text(),
            'create_time' => $this->datetime(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('posts');
    }
}
