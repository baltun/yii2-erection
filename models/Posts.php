<?php

namespace app\models;

use dektrium\user\models\User;
use Yii;
use app\models\Profile as UserProfile;


/**
 * This is the model class for table "posts".
 *
 * @property integer $id
 * @property string $preview
 * @property string $text
 * @property string $create_time
 */
class Posts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'posts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['preview', 'text', 'create_time'], 'required'],
            [['preview', 'text'], 'string'],
            [['create_time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'preview' => 'Preview',
            'text' => 'Text',
            'create_time' => 'Create Time',
        ];
    }

    public function beforeSave($insert){

        // отправляем письма по email
            $emailSendStatus = Yii::$app->mailer->compose()
                ->setFrom('info@gsoft.ru')
                ->setTo($this->getEmails())
                ->setSubject('Тут еще')
                ->setTextBody('Новая новость! '.$this->getAttribute('preview'))
                ->setHtmlBody('<b>Новая новость!</b><br/><br/>'.$this->getAttribute('preview'))
                ->send();

        //отправляем push-уведомление подписавшимся

        $pushAll = new PushAll([
            'id' => "3127",
            'key' => "698bec0e3b4992764366c8d651cb7fd7",
            'title' => 'Еще одно!',
            'text' => 'Новая новость: '.$this->getAttribute('preview'),
            'url' => 'http://google.com/q='.$this->getAttribute('preview'),
            'type' => 'multicast',
            'uids' => $this->getPushAllIDs(),
        ]);

        $result = $pushAll->send();


        return parent::beforeSave($insert);
    }


    private function getPushAllIDs(){
        $profiles = new UserProfile();

        $pushallids = $profiles
            ->find()
            ->where('pushalluserid is NOT NULL')
            ->andWhere('pushall_subscribe = 1')
            ->asArray()
            ->all();

        $result = '[';
        foreach ($pushallids as $key => $value) {
            $result .= $value['pushalluserid'];
            if (next($pushallids) == true) $result .= ', ';
        }
        $result .= ']';

        return $result;
    }

    public function getUser(){
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    private function getEmails(){
        $profiles = UserProfile::find()
            ->with('user')
            ->all();
        foreach ($profiles as $profile) {
            $user = $profile->user;
            $result[] = $user->email;
        }
        return $result;
    }
}
